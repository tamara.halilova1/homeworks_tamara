let object = {
    value: 1,
    next: {
      value: ['это', 'массив'],
      next: {
          value: new Date(),
          next: {
            value: 4,
            next: null
        }
      }
    }
};
    
function clone(list) {
    let objectClone = {};

    for (key in list) {
        objectClone[key] = list[key];
      }
      return objectClone;
    
}
    
let newO = clone(object);
console.log(newO);

object.value = 'изменила значение свойства изначального массива';

console.log(object); 
console.log(clone(newO)); // в клон это значение уже не перенеслось
