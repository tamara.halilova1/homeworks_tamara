let student = {};

student.name = prompt('Твоё имя:');
student['last name'] = prompt('Твоя фамилия:');
student.table = {};

let subject;
let mark;

do {
    subject = prompt('Предмет:'); 
    mark = +prompt('Оценка:'); 

    if(subject && mark) {
        student.table[subject] = mark;
    }

} while (subject);

console.log(student);

let badMarksQuantity = 0;
let marksSum = 0;
let marksQuantity = 0;

for (let key in student.table) {
    if(student.table[key] < 4) {
        ++badMarksQuantity;
    }

    marksSum += student.table[key];
    ++marksQuantity;
}

if(!badMarksQuantity) {
    console.log('Студент переведен на следующий курс');
}

let averageMark = Math.round(marksSum/marksQuantity);

if(averageMark > 7) {
  console.log(`Студенту назначена стипендия`);  
}
