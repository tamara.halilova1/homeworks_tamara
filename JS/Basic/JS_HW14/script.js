'use strict'

const $ = window.$;

$(document).ready(function(){

    $(".navigation").on("click","a", function () {
        const topCoordinate  = $($(this).attr('href')).offset().top;
        $('html').animate({scrollTop: topCoordinate}, 1000);
    });

    const btn = $('<button class="arrow">Наверх</button>').appendTo('body');
    btn.css({
        'border': '1px solid red',
        'background':'grey', 
        'padding':'10px',
        'position':'fixed',
        'top':'70%',    
        'left':'90%',
        'display': 'none'    
    });

    $(window).on('scroll', function() {
        ($(window).scrollTop() >= $(window).height()) ? btn.css('display', '') : btn.css('display', 'none');
    });
    
    $(document).on('click', '.arrow', function() {
        $('html').animate({scrollTop: 0}, 1000);
    });
    
    $('.slide-toggler').css({
        'border': '1px solid red',
        'background':'yellow', 
        'padding':'10px',
        'position':'absolute',
        'bottom': '0',    
    });

    $(document).on('click', '.slide-toggler', function (){
        $('#posts').slideToggle();
    });

});

