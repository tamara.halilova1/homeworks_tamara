function getEstimation(developersSpeed, backlog, dd) {

    let pointsPerDay = 0;
    for (let arrayElement = 0; arrayElement <= developersSpeed.length - 1; arrayElement++) {
        pointsPerDay += developersSpeed[arrayElement];
    }

    let totalTacks = 0;
    for (let arrayElement = 0; arrayElement <= backlog.length - 1; arrayElement++) {
        totalTacks += backlog[arrayElement];
    }

    let projectDays = totalTacks/pointsPerDay;

    if (projectDays >= 7) {
        projectDays += 2*(projectDays/7)
    }

    let endDate = +new Date() + projectDays*24*60*60*1000;

    if ( +dd >= +endDate ) {
        console.log(`Все задачи будут успешно выполнены за ${Math.ceil((dd - endDate)/24/60/60/1000)} дней до наступления дедлайна!`)
    } else {
        console.log(`Команде разработчиков придется потратить дополнительно ${Math.ceil((endDate - dd)/60/60/1000)} часов после дедлайна, чтобы выполнить все задачи в беклоге`)
    }
}

getEstimation([5, 3, 4], [150, 14, 5], new Date('2020-05-30')); //тестовый вызов
