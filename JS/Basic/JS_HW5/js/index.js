let firstName = prompt("Pls enter ur name");
let lastName = prompt("Pls enter ur surname");
let birthday = prompt("Pls enter ur date of birth (in format: dd.mm.yyyy)");


function createNewUser() {

  return {
    firstName,
    lastName,
    birthday,

    getAge() {
      if (this.birthday !== null) {
        let array = this.birthday.split('.');
        // let passedTime = new Date() - new Date(array[2] + '-' + array[1] + '-' + array[0]);
        
        let passedTime = new Date() - new Date(array.reverse().join('.'));
        return `Your age is ${Math.floor(passedTime/1000/60/60/24/365)}.`;
      }
      
    },

    getPassword() {
      if (this.birthday !== null) {
        let array = this.birthday.split('.');
        return `Your password can be '${(this.firstName.substr(0, 1)).toLowerCase() + this.lastName.toLowerCase() + array[2]}'` 
      }
    },

  }
}

console.log(createNewUser().getAge(), createNewUser().getPassword()) ;