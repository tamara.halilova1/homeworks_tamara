'use strict'

const mainBox = document.querySelector('.main-container');
const links = Array.from(document.querySelectorAll('a'));
const img = document.querySelector('img');
const initialImg = './img/img1.png';
const newImg = './img/img2.png';

document.addEventListener('DOMContentLoaded', () => {

    document.addEventListener('click', setTheme);
    function setTheme(event) {
        
        if(!event.target.classList.contains('toggler')) return;
        event.preventDefault();
    
        mainBox.classList.toggle('pink-theme');
        links.forEach(li => li.classList.toggle('pink-theme'));  
        
        const theme = mainBox.classList[1];
        (theme)? localStorage.setItem('theme', theme) : localStorage.setItem('theme', '');
    
        (img.getAttribute('src') === initialImg)? img.setAttribute('src', newImg): img.setAttribute('src', initialImg);
        localStorage.setItem('img', img.getAttribute('src'));
    }
    
    if (localStorage.getItem('theme')) {
        mainBox.classList.add(localStorage.getItem('theme'));
        links.forEach(li => li.classList.add(localStorage.getItem('theme')));
    }
    
    (localStorage.getItem('img')) ? img.setAttribute('src', localStorage.getItem('img')) : false;
});



