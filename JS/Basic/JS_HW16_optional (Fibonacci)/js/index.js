let n;

do {
   n = prompt('ur num'); 
} while (n === null || isNaN(n));

function fibonacci(n) {
     if (n < 0) {
        if (n === -1) {
            return 1
            } else {
                return fibonacci(n + 2) - fibonacci(n + 1)
            }
        }

        else if (n > 0) {
            if (n === 1 || n === 2) {
                return 1
            } else {
                return fibonacci(n - 1) + fibonacci(n - 2)
            }    

        } else if (n === 0 ) {
            return 0
        }
}

console.log(fibonacci(+n));    


// Пометка себе:
    // fib(-1) = fib(-2) + fib(-3) = -1 + 2 = 1
    // fib(0) = fib(-1) + fib(-2) = 1 + -1
    // fib(1) = 1 = fib(0) + fib(-1) = 0 + 1
    // fib(2) = 1 = fib(1) + fib(0) = 1 + 0
    // fib(3) = fib(2) + fib(1)
    // fib(4) = fib(3) + fib(2) = 2 + 1 = 3
    // fib(5) = fib(4) + fib(3) = 3 + 2 = 5
    // fib(6) = fib(5) + fib(4) = 5 + 3 = 8


