Метод **ForEach** принимает в качестве параментрa функцию (чаще всего коллбек), которая в свою очередь принимает элемент массива (а также, необязательные параметры - его индекс и сам массив).

Метод ничего не возвращает, а только осуществляет заданные действия, приписанные в теле коллбека;
С псевдомассивами не работает, кроме DOM-коллекций, полученных от методов .querySelectorAll() и .getElementsByName().




