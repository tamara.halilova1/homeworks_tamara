let number1 = prompt("Pls enter ur 1st num");
let number2 = prompt("Pls enter ur 2d num");
let operation = prompt("Pls enter chosen math operation: +, -, *, /");

if(number1 === null || number2 === null || !isFinite(number1) || !isFinite(number2)) {
    while (number1 === null || !isFinite(number1)) {
        number1 = prompt("Pls enter ur 1st num", number1);
    }

    while (number2 === null || !isFinite(number2)) {
        number2 = prompt("Pls enter ur 2d num", number2);
    }
}
function action(number1, number2, operation) {
  switch (operation) {
    case "+":
      return number1 + number2;
    case "-":
      return number1 - number2;
    case "*":
      return number1 * number2;
    case "/":
      return number1 / number2;

    default:
      "I don't know this operation";
  }
}

let result = action(+number1, +number2, operation);
console.log(result);