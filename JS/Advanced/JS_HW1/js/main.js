// TASK 1
const arr = ["Kyiv", "Berlin", "Dubai", "Moscow", "Paris"];

const [UA, DE, AE, RU, FR] = arr;
console.log(`Task 1: ${UA} ${DE}, ${AE}, ${RU}, ${FR}`);

// TASK 2
const employee = {
    name: 'Tamara',
    salary: '1mUSD',
}

const {name, salary} = employee;
console.log(`Task 2: emloyee name - ${name}; emloyee salary - ${salary}`);

// TASK 3
const array = ['value', 'showValue'];
const [value, showValue] = array;

alert(value); // будет выведено 'value'
alert(showValue);  // будет выведено 'showValue'

