document.addEventListener('submit', (e)=>{
    e.preventDefault();

    fetch('https://api.ipify.org/?format=json')
    .then(res => { 
        if(!res.ok) throw new Error('ошибка');
            return res.json()
    })
    .then(res=>{
        fetch(`http://ip-api.com/json/${res.ip}?fields=status,message,continent,country,regionName,city,district,query`) 
        .then(response => { 
            if(!response.ok) throw new Error('ошибка');
                return response.json()
        })
        .then(response=>{
            const div = document.createElement('div');
            div.textContent = `Информация: ${response.continent}, ${response.country}, ${response.regionName}, ${response.city}`;
            document.body.append(div);
        })
    });
})