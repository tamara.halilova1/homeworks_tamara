let books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];

const root = document.createElement('div');
const ul = document.createElement('ul');
root.setAttribute("id", "root");

document.body.prepend(root);
root.prepend(ul);

for (let i = 0; i < books.length; i++) {
    try{
        if (books[i].author && books[i].name && books[i].price) {
            books[i] = Object.values(books[i]).join();
    
            let li = document.createElement('li');
            li.innerText = books[i];
            ul.prepend(li);
        } else {
            throw new Error(`property was missed`);
        }
    } catch(err){
        if (err.message === `property was missed`){
            !books[i].author? console.log(`missed property is: author`) :
            !books[i].name? console.log(`missed property is: name`) :
            !books[i].price? console.log(`missed property is: price`) : 
            null;
        }
    }
}