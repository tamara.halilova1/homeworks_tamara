class Employee {
    constructor(name, age, salary){
        this._name = name;        
        this._age = age;
        this._salary = salary;
    }
    
    //геттер & cеттер для свойства name
    set objName(newName){
        this._name= newName;
    }
    get objName(){
        return this._name;
    }

    //геттер & cеттер для свойства age
    set objAge(newAge){
        this._age= newAge;
    }
    get objAge(){
        return this._age;
    }

    //геттер & cеттер для свойства salary
    set objSalary(newSalary){
        this._salary= newSalary;
    }
    get objSalary(){
        return this._salary;
    }

}

const employer = new Employee('Тамара', '25', '1000');
console.log(`Изначальная зп объекта класса Employee: ${employer._salary}$`);

//изменённая зп класса Employee
employer.objSalary = '5000'
console.log(`Изменённая зп объекта класса Employee: ${employer._salary}$`);



class Programmer extends Employee{
    constructor(name, age, salary, lang){
        super(name, age, salary)
        this._lang = lang
    }

    set objSalary(salary){
        super._salary = salary;
    }
    get objSalary(){
        return this._salary *= 3;
    }
}

const programmer1 = new Programmer('Тома', '26', '100000', 'js');
const programmer2 = new Programmer('Вася', '50', '2000', 'morse');
const programmer3 = new Programmer('Петя', '60', '3000', 'chineese');

//проверка изменений в классе Programmer
console.log(`Увеличенная в 3 раза зп объекта класса Programmer: ${programmer1.objSalary}$`);


console.log("вывод в консоль экземляров класса Programmer:");
console.log(programmer1);
console.log(programmer2);
console.log(programmer3);




