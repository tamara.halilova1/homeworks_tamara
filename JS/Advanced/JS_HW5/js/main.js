 const button = document.querySelector('button');
 
 button.addEventListener('click', async (e)=>{
    e.preventDefault();
    
    const response = await fetch('https://api.ipify.org/?format=json');
    const ipObj = await response.json();

    const responseDetailed = await fetch(`http://ip-api.com/json/${ipObj.ip}?fields=status,message,continent,country,regionName,city,district,query`);
    const ipDetailed = await responseDetailed.json();

    const div = document.createElement('div');
    div.textContent = `Информация: ${ipDetailed.continent}, ${ipDetailed.country}, ${ipDetailed.regionName}, ${ipDetailed.city}`;
    document.body.append(div);
})