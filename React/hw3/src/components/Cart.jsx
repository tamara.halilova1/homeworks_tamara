import React from 'react';
import {ProductCard} from './ProductCard';

export const Cart = ({productsArr, cart, productCardProps}) => {
    return(
      <ul className='cards-container'>
        {productsArr.map( el => (cart.includes(el.code)) && (<ProductCard closeButton={true} key={el.code} {...el} {...productCardProps} text='Add to cart'/>))}
      </ul>
    )
}
