import React from 'react';
import {ProductCard} from './ProductCard';

export const Homepage = ({productsArr, productCardProps}) => {
    return(
        <ul className='cards-container'>
            {productsArr.map( el => <ProductCard key={el.code} {...el} {...productCardProps} text='Add to cart'/> ) }
        </ul>
    )
}
