import React from 'react';
import PropTypes from 'prop-types';

export const Modal = ({header, closeButton, text, actions, className, onClick}) => {

    return(
        <div className={className}>
            <header>
                <p>{header}</p>
                {closeButton && <span className='btn--close' onClick={onClick}>x</span>}
            </header>
            <main>
                <p>{text}</p>
                {actions}
            </main>
        </div>
    )
}

Modal.propTypes = {
    header: PropTypes.string,
    closeButton: PropTypes.bool,
    text: PropTypes.string,
    actions: PropTypes.object,
}

Modal.defaultProps = {
    header: "Are you sure you want to delete this card from the cart?",
    closeButton: true,
    text: "Please choose an answer",
    className: 'modal'
}