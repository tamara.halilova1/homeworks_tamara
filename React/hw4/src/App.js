import React, {useEffect} from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";

import { connect } from 'react-redux';

import {Cart} from './components/Cart';
import {Favorites} from './components/Favorites';
import {Homepage} from './components/Homepage';
import {Navigation} from './components/Navigation';
import {Button} from './components/Button';
import {Modal} from './components/Modal';
import { getProducts, setModalState, setModalDeleteCardState, setAppearance, setDeletedCard, setFavorites, setCart } from './store/store';
import './App.scss';

const mapStateToProps = state => ({
  products: state.products,
  modalState: state.modalState,
  modalDeleteCardState: state.modalDeleteCardState,
  appearance: state.appearance,
  deletedCard: state.deletedCard,
  favorites: state.favorites,
  cart: state.cart,
})

const App = connect(mapStateToProps, { getProducts, setModalState, setModalDeleteCardState, setAppearance, setDeletedCard, setFavorites, setCart }) ( ({getProducts, products, setModalState, modalState, setModalDeleteCardState, modalDeleteCardState, appearance, setAppearance, deletedCard, setDeletedCard, favorites, setFavorites, cart, setCart}) =>{

  useEffect(()=>{
    getProducts();
  }, []);

  
  const deleteFromCart = (event)=>{
    if(event.target.classList.contains('btn--yes')){
      setCart(cart.filter(el=>el !== +deletedCard) );

      localStorage.setItem('cart', JSON.stringify([...(cart.filter(el => el !== +deletedCard)) ]));  
    }

    setDeletedCard('');

    setModalDeleteCardState(!modalDeleteCardState);

    (!modalDeleteCardState) ? getDisabled() : unBlur();
  }

  function modalHandler(event){
    event.stopPropagation();

    setModalState(!modalState);
    (!modalState) ? getDisabled() : unBlur();

    if(event.target.textContent !== 'Ok'){
      cart.pop();
      localStorage.setItem('cart', JSON.stringify(cart));
      return;
    } 

    localStorage.setItem('cart', JSON.stringify(cart))
  }

  function modalDeleteCardHandler(event){
    event.stopPropagation();

    setDeletedCard(event.target.closest('li').id);

    setModalDeleteCardState(!modalDeleteCardState);
    (!modalDeleteCardState) ? getDisabled() : unBlur(); 
  }

  const getDisabled = ()=> setAppearance(' disabled');  
  function unBlur(){

    setAppearance('');

    if(modalState) setModalState(!modalState);
    else if(modalDeleteCardState) setModalDeleteCardState(!modalDeleteCardState);
  }

  const productCardProps = {
    className: `btn btn--choose`,
    modalHandler,
    modalDeleteCardHandler,

    cart,
    favorites,
    setCart,
    setFavorites,
    actions: <div className='btn-container'>
      <Button className='btn btn--yes' text='Yes' onClick={deleteFromCart}></Button>
      <Button className='btn btn--close' text='No' onClick={deleteFromCart}></Button>
      </div>
  }

  const modalContent = {
    className: 'modal',
    header:'Do you want to add this card to the...cart?',
    closeButton: true,
    onClick: modalHandler,
    text:`Once you approve this action, this action will be done.
    Are you sure you want to do this?`,
    actions: <div className='btn-container'>
                <Button className='btn' text='Ok' onClick={modalHandler}></Button>
                <Button className='btn btn--close btn--cancel' text='Cancel' onClick={modalHandler}></Button>
              </div>
  }

  return (
        <Router>
          <div className="App">
            
            <div className={'wrapper' + appearance} onClick={unBlur}>

              <Navigation/>

              <Switch>
                <Route path='/cart'>
                  <Cart products={products} cart={cart} productCardProps={productCardProps}/>
                </Route>
                <Route path='/favorites'>
                  <Favorites products={products} favorites={favorites} productCardProps={productCardProps}/>
                </Route>
                <Route path='/'>
                  <Homepage products={products} productCardProps={productCardProps}/>
                </Route>
              </Switch>

            </div>

            {modalState && <Modal {...modalContent} />}
            {modalDeleteCardState && <Modal onClick={deleteFromCart} actions={productCardProps.actions}/>}
            
          </div>
        </Router>
  )
})

export default App;
