import React from 'react';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk'

//REDUCER
const initialState = {
    products: [{
        "name": "Indian red admiral",
        "price":  "14.99$",
        "url": "/img/news10.jpg",
        "code": 10,
        "color": "#7FB3D5"
    }],
    modalState: false,
    modalDeleteCardState: false,
    appearance: '',
    deletedCard: '',
    favorites: checkFavorites(),
    cart: checkCart()
}

export function reducer(state = initialState, {type, payload}){

    switch(type){
        case 'SET_PRODUCTS':
            return {
                ...state,
                products: [...payload]
            }
        case 'HANDLE_MODAL':
            return {
                ...state,
                modalState: payload
            }
        case 'HANDLE_MODAL_DELETE':
            return {
                ...state,
                modalDeleteCardState: payload
            }
        case 'SET_APPEARANCE':
            return {
                ...state,
                appearance: payload
            }
        case 'SET_DELETED_CARD':
            return {
                ...state,
                deletedCard: payload
            }
        case 'SET_FAVORITES':
            return {
                ...state,
                favorites: [...payload]
            }
        case 'SET_CART':
            return {
                ...state,
                cart: [...payload]
            }

        default:
            return state
    }
}

// MIDDLEWARE

export const getProducts = ()=> async (dispatch)=>{
    try{
        const res = await fetch('../products.json');
        const data = await res.json();
        dispatch(setProducts(data));
    } catch(e){
        console.log(e.message)
    }

  }

// ACTION CREATORS
export const setProducts = payload => ({
    type: 'SET_PRODUCTS',
    payload
})
export const setModalState = payload => ({
    type: 'HANDLE_MODAL',
    payload
})
export const setModalDeleteCardState = payload => ({
    type: 'HANDLE_MODAL_DELETE',
    payload
})
export const setAppearance = payload => ({
    type: 'SET_APPEARANCE',
    payload
})
export const setDeletedCard = payload => ({
    type: 'SET_DELETED_CARD',
    payload
})
export const setFavorites = payload => ({
    type: 'SET_FAVORITES',
    payload
})
export const setCart = payload => ({
    type: 'SET_CART',
    payload
})


// STORE
export const store = createStore(reducer, applyMiddleware(thunk));



//additional
function checkFavorites(){
    if (localStorage.getItem('favorites')) return JSON.parse(localStorage.getItem('favorites'));
    return [];
}
function checkCart(){
    if (localStorage.getItem('cart')) return JSON.parse(localStorage.getItem('cart'));
    return [];
}