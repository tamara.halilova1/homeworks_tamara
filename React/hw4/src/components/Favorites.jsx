import React from 'react';
import {ProductCard} from './ProductCard';

export const Favorites = ({products, favorites, productCardProps}) => {
    return(
      <ul className='cards-container'>
        {products.map( el => (favorites.includes(el.code)) && (<ProductCard key={el.code} {...el} {...productCardProps} text='Add to cart'/>))}
      </ul>
    )
}