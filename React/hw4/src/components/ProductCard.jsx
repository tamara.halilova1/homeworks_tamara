import React, {useState} from 'react';
import PropTypes from 'prop-types';
import {Button} from './Button';


export const ProductCard = ({url, name, price, color, modalDeleteCardHandler, modalHandler, text, code, favorites, setFavorites, closeButton, cart, setCart}) => {
    const [isFavourite, setisFavourite] = useState(check);

    function check(){
        if (favorites.includes(code)) {
            return true 
            } else { 
            return false
        };      
    }

    const addToCart = (event)=>{
        setCart([...cart, code]);
        modalHandler(event);
    }    
    const addFavorites = ()=>{
        setFavorites([...favorites, code]);
        localStorage.setItem('favorites', JSON.stringify([...favorites, code]));
    }    

    const removeFromFavorites = ()=>{
        setFavorites(favorites.filter(el => el !== code));
        localStorage.setItem('favorites', JSON.stringify([...(favorites.filter(el => el !== code))]));
    }    

    const changeStar = ()=>{
        setisFavourite(!isFavourite);
        (!isFavourite) ? addFavorites() : removeFromFavorites();
    }

   return(
       <li id={code} className='card' style={{backgroundColor: color}}>
           {closeButton && <p className='btn--x' onClick={modalDeleteCardHandler}>X</p>}

           <img alt='картинка' src={url}/>
           {isFavourite ? <i className='fas fa-star' onClick={changeStar}></i> : <i className='far fa-star' onClick={changeStar}></i>}
           <h3>{name}</h3>
           <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit.</p>
           <div className='card-footer'>
               <p style={{fontWeight: 700}}>{price}</p>
               {cart.includes(code) || <Button onClick={addToCart} text={text}/>}
           </div>
       </li>
   )
}

ProductCard.propTypes = {
    url: PropTypes.string,
    name: PropTypes.string,
    price: PropTypes.string,
    color: PropTypes.string,
    code: PropTypes.number,
}

ProductCard.defaultProps = {
    text: 'Lorem ipsum'
}
