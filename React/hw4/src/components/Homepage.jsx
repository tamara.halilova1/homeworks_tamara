import React from 'react';
import {ProductCard} from './ProductCard';

export const Homepage = ({products, productCardProps}) => {
    return(
        <ul className='cards-container'>
            {products.map( el => <ProductCard key={el.code} {...el} {...productCardProps} text='Add to cart'/> ) }
        </ul>
    )
}
