import React from 'react';
import {Link} from "react-router-dom";

export const Navigation = () => {

    return(
        <nav>
            <ul className='navigation'>
                <li><Link className='navlink' to='/'>Homepage</Link></li>
                <li><Link className='navlink' to='/cart'>Shopping cart</Link></li>
                <li><Link className='navlink' to='/favorites'>Favorites</Link></li>
            </ul>
        </nav> 
    )

}
    