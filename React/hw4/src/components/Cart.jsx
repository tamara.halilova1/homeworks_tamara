import React from 'react';
import {ProductCard} from './ProductCard';

export const Cart = ({products, cart, productCardProps}) => {
    return(
      <ul className='cards-container'>
        {products.map( el => (cart.includes(el.code)) && (<ProductCard closeButton={true} key={el.code} {...el} {...productCardProps} text='Add to cart'/>))}
      </ul>
    )
}
