import React from 'react';

export const Modal = ({header, closeButton, text, actions, className, onClick}) => {
    return(
        <div className={className}>
            <header>
                <p>{header}</p>
                {closeButton && <span className='btn--close' onClick={onClick}>x</span>}
            </header>
            <main>
                <p>{text}</p>
                {actions}
            </main>
        </div>
    )
}
