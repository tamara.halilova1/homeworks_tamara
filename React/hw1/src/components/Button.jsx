import React from 'react';

export const Button = ({text, backgroundColor, className, onClick})=>{
    return(
    <button className={className} onClick={onClick} style={{backgroundColor: backgroundColor}}>{text}</button> 
    )
}