import React, {useState} from 'react';
import {Button} from './components/Button';
import {Modal} from './components/Modal';
import './App.scss';

function App() {
  const [modalTemplateState, setModalTemplate] = useState(false);
  const [modalCustomState, setModalCustom] = useState(false);
  const [appearance, setAppearance] = useState('');

  const getDisabled = ()=> setAppearance(appearance=>appearance=' disabled');

  function unBlur(){
    setAppearance(appearance=>appearance='');

    setModalTemplate(modalTemplateState => modalTemplateState = false);
    setModalCustom(modalCustomState => modalCustomState = false);
  }
  function modalTemplateOpen(event){
    event.stopPropagation();

    setModalTemplate(modalTemplateState => !modalTemplateState);
    (!modalTemplateState) ? getDisabled() : unBlur();

    if(modalCustomState) setModalCustom(modalCustomState => !modalCustomState);
  }
  function modalCustomOpen(event){
    event.stopPropagation();

    setModalCustom(modalCustomState => !modalCustomState);
    (!modalCustomState) ? getDisabled() : unBlur();

    if(modalTemplateState) setModalTemplate(modalTemplateState => !modalTemplateState);
  }
  const btnTemplate = {
    className: `btn btn--choose`,
    text:'Open first modal', 
    backgroundColor:'lightblue',
    onClick: modalTemplateOpen,
  }

  const btnCustom = {
    className: 'btn btn--choose',
    text:'Open second modal', 
    backgroundColor:'lightpink',
    onClick: modalCustomOpen,
  }

  const br = <br/>;

  const modalTemplateContent = {
    className: 'modal',
    header:'Do you want to delete this file?',
    closeButton: true,
    onClick: modalTemplateOpen,
    text:`Once you delete this file, it won’t be possible to undo this action.
    
    Are you sure you want to delete it?`,
    actions: <div className='btn-container'>
                <Button className='btn' text='Ok'></Button>
                <Button className='btn btn--close' text='Cancel' onClick={modalTemplateOpen}></Button>
              </div>
  }

  const modalCustomContent = {
    className: 'modal',
    header:'Are you sure you want to delete this?',
    closeButton: true,
    onClick: modalCustomOpen,
    text:`London is the capital of Great Britain`,
    actions: <div className='btn-container'>
              <Button className='btn btn--custom' text='Yes'></Button>
              <Button className='btn btn--custom btn--close' text='No' onClick={modalCustomOpen}></Button>                  
            </div>
  }

  return (
    <div className="App">
      <div className={'wrapper' + appearance} onClick={unBlur}>
        <Button {...btnTemplate} />
        <Button {...btnCustom} />
      </div>

      {modalTemplateState && <Modal {...modalTemplateContent} />}
      {modalCustomState && <Modal {...modalCustomContent} />}

    </div>
  );
}

export default App;
