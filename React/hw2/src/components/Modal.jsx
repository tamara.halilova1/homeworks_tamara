import React from 'react';
import PropTypes from 'prop-types';

export const Modal = ({header, closeButton, text, actions, className, onClick}) => {
    return(
        <div className={className}>
            <header>
                <p>{header}</p>
                {closeButton && <span className='btn--close' onClick={onClick}>x</span>}
            </header>
            <main>
                <p>{text}</p>
                {actions}
            </main>
        </div>
    )
}

Modal.propTypes = {
    header: PropTypes.string,
    closeButton: PropTypes.bool,
    text: PropTypes.string,
    actions: PropTypes.object,
}

Modal.defaultProps = {
    className: '___',
    onClick: console.log('privet)))))')
}