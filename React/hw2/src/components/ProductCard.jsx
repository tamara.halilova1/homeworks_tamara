import React, {useState} from 'react';
import PropTypes from 'prop-types';
import {Button} from './Button';

let favorites = [];

export const ProductCard = ({url, name, price, color, onClick, text, code}) => {
    const [starClass, setStarClass] = useState('far fa-star');
    
    const addFavorites = (event)=>{
        favorites.push(event.target.closest('li').id);

        localStorage.setItem('favorites', JSON.stringify(favorites));
    }    
    const removeFromFavorites = (event)=>{
        favorites.push(event.target.closest('li').id);
        favorites = favorites.filter(el => el !== event.target.closest('li').id);

        localStorage.setItem('favorites', JSON.stringify(favorites));
    }    
    const changeStar = (event)=>{
        if(starClass === 'far fa-star'){
            setStarClass('fas fa-star');
            addFavorites(event);
        } else {
            setStarClass('far fa-star');
            removeFromFavorites(event);
        }
    }

   return(
       <li id={code} className='card' style={{backgroundColor: color}}>
           <img alt='картинка' src={url}/>
           <i className={starClass} onClick={changeStar}></i>
           <h3>{name}</h3>
           <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit.</p>
           <div className='card-footer'>
               <p >{price}</p>
               <Button onClick={onClick} text={text}/>
           </div>
       </li>
   )
}

ProductCard.propTypes = {
    url: PropTypes.string,
    name: PropTypes.string,
    price: PropTypes.string,
    color: PropTypes.string,
    code: PropTypes.number,
}

ProductCard.defaultProps = {
    text: 'Lorem ipsum'
}