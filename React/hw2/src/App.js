import React, {useState, useEffect} from 'react';
import {Button} from './components/Button';
import {Modal} from './components/Modal';
import {ProductCard} from './components/ProductCard';
import './App.scss';

let cart = [];

function App() {
  const [modalState, setModalState] = useState(false);
  const [appearance, setAppearance] = useState('');
  const [productsArr, setProductsArr] = useState([]);

  const getProductsArr = async ()=>{
    const res = await fetch('./products.json');
    const data = await res.json();
    setProductsArr(data);
  }

  useEffect( ()=>{
    getProductsArr() //выполняется при рендере только 1 раз
  }, []); 

  const addToCart = ()=>{
    localStorage.setItem('cart', JSON.stringify(cart));

    setModalState(modalState => !modalState);
    (!modalState) ? getDisabled() : unBlur();
  }   

  function modalHandler(event){
    event.stopPropagation();

    setModalState(modalState => !modalState);
    (!modalState) ? getDisabled() : unBlur();

    if(event.target.classList.contains('btn--close')){
      cart.pop();
      localStorage.setItem('cart', JSON.stringify(cart));
      return;
    } 
    const cardIt = event.target.closest('li').id;
    if(!cart.includes(cardIt)) cart.push(cardIt);  
  }

  const getDisabled = ()=> setAppearance(appearance=>appearance=' disabled');  
  function unBlur(){
    setAppearance(appearance=>appearance='');

    setModalState(modalState => modalState = false);
  }

  const btn = {
    className: `btn btn--choose`,
    onClick: modalHandler,
}

  const modalContent = {
    btn,
    className: 'modal',
    header:'Do you want to add this card to the...cart?',
    closeButton: true,
    onClick: modalHandler,
    text:`Once you approve this action, this action will be done.
    Are you sure you want to do this?`,
    actions: <div className='btn-container'>
                <Button className='btn' text='Ok' onClick={addToCart}></Button>
                <Button className='btn btn--close' text='Cancel' onClick={modalHandler}></Button>
              </div>
  }

  return (
    <div className="App">
      
      <div className={'wrapper' + appearance} onClick={unBlur}>
        <ul className='cards-container'>
          {productsArr.map( el => <ProductCard key={el.code} {...el} {...btn} text='Add to cart'/> ) }
        </ul>
      </div>

      {modalState && <Modal {...modalContent} />}

    </div>
  );
}

export default App;


